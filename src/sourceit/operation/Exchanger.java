package sourceit.operation;

public interface Exchanger {
    double convert(int money,String buyOrSell,String currency);
}

