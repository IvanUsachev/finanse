package sourceit.Organizations;

import sourceit.KreditDepositSend.Kredit;
import sourceit.Organization;

public class KreditKafe extends Organization implements Kredit {
    int maxSumma;
    float kreditPersent;

    public KreditKafe(String name, String addres, int maxSumma, float kreditPersent) {
        super(name, addres);
        this.maxSumma = maxSumma;
        this.kreditPersent = kreditPersent;
    }

    @Override
    public double kredit(int moneyKredit) {
        if (moneyKredit > maxSumma) {
            return 0;
        } else {
            return moneyKredit * kreditPersent;
        }

    }
}
