package sourceit;


import sourceit.operation.Deposit;
import sourceit.operation.Exchanger;
import sourceit.operation.Kredit;
import sourceit.operation.Send;
import sourceit.firm.Organization;
import java.util.ArrayList;


public class FinanceSystem {
    ArrayList organizations;


    public FinanceSystem(ArrayList organizations) {
        this.organizations = organizations;
    }
    public void yes() {
        System.out.println("Подходящий вариант по указаным критериям: ");
    }
    public void no() {
        System.out.println("Подходящий вариант по указаным критериям: ");
    }
    public void printBestSender(int moneySend) {
        Organization bestSender = null;
        double bestValue = 0;
        for (Object organization : organizations) {
            if (organization instanceof Send) {
                double value = ((Send) organization).send(moneySend);
                if (bestValue == 0 || bestValue < value) {
                    bestSender = (Organization) organization;
                    bestValue = value;
                }
            }
        }
        if (bestSender != null) {
            yes();
            bestSender.print();
        } else {
            no();
        }
    }
    public void printBestDeposit(int moneyDeposit, int moneymoneyMonth) {
        Organization bestDeposit = null;
        double bestValue = 0;
        for (Object organization : organizations) {
            if (organization instanceof Deposit) {
                double value = ((Deposit) organization).deposit(moneyDeposit, moneymoneyMonth);
                if (bestValue == 0 || bestValue < value) {
                    bestDeposit = (Organization) organization;
                    bestValue = value;
                }
            }
        }
        if (bestDeposit != null) {
            yes();
            bestDeposit.print();
        } else {
            no();
        }
    }
    public void printBestKredit(int moneyKredit) {
        Organization bestKredit = null;
        double bestValue = 0;
        for (Object organization : organizations) {
            if (organization instanceof Kredit) {
                double value = ((Kredit) organization).kredit(moneyKredit);
                if (bestValue == 0 || bestValue > value) {
                    bestKredit = (Organization) organization;
                    bestValue = value;
                }
            }
        }
        if (bestKredit != null) {
            yes();
            bestKredit.print();
        } else {
            no();
        }
    }
    public void printBestExchanger(String currency, String buyOrSell, int money) {
        Organization bestExchanger = null;
        double bestValue = 0;
        for (Object organization : organizations) {
            if (organization instanceof Exchanger) {
                double value = ((Exchanger) organization).convert(money, buyOrSell, currency);
                if (bestValue == 0 || bestValue < value) {
                    bestExchanger = (Organization) organization;
                    bestValue = value;
                }
            }
        }
        if (bestExchanger != null) {
            yes();
            bestExchanger.print();
        } else {
            no();
        }
    }
}

