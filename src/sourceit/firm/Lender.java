package sourceit.firm;

import sourceit.operation.Kredit;

public class Lender extends Organization implements Kredit {
    int maxSumma;
    float kreditPersent;

    public Lender(String name, String addres, int maxSumma, float kreditPersent) {
        super(name, addres);
        this.maxSumma = maxSumma;
        this.kreditPersent = kreditPersent;
    }

    @Override
    public double kredit(int moneyKredit) {
        if (moneyKredit > maxSumma) {
            return 0;
        } else {
            return moneyKredit * kreditPersent;
        }

    }

    @Override
    public void print() {
        super.print();
        System.out.println("Максимальная сумма кредита: " + maxSumma);
        System.out.println("Процент по кредиту: " + kreditPersent);
    }
}
