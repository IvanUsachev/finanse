package sourceit.firm;

public class Organization {
    String name;
    String addres;

    public Organization(String name, String addres) {
        this.name = name;
        this.addres = addres;
    }

    public void print() {
        System.out.println("Организация: " + name);
        System.out.println("Адрес: " + addres);
    }

    public String getName() {
        return name;
    }

    public String getAddres() {
        return addres;
    }


}