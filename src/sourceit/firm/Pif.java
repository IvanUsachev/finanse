package sourceit.firm;

import sourceit.operation.Deposit;

public class Pif extends Organization implements Deposit {
    int yearOpen;
    final static int MINDEPOSITMONTH = 12;

    public Pif(String name, String addres, int yearOpen) {
        super(name, addres);
        this.yearOpen = yearOpen;
    }

    public double deposit(int moneyDeposit, int moneyMonth) {
        if (moneyMonth <= MINDEPOSITMONTH) {
            return 0;
        } else {
            return moneyDeposit;
        }
    }

    @Override
    public void print() {
        super.print();
        System.out.println("Год открытия: " + yearOpen);
    }
}
