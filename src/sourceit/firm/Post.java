package sourceit.firm;

import sourceit.operation.Send;

public class Post extends Organization implements Send {
    final static float KOMISIASEND = 0.02f;
    public Post(String name, String addres) {
        super(name, addres);
    }
    @Override
    public double send(int moneySend) {
        return moneySend * KOMISIASEND;
    }
}
