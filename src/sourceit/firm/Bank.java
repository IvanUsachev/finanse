package sourceit.firm;


import sourceit.operation.Deposit;
import sourceit.operation.Exchanger;
import sourceit.operation.Kredit;
import sourceit.operation.Send;


public class Bank extends Organization implements Send, Deposit, Kredit, Exchanger {

    final static int LIMIT = 12_000;
    final static int KOMISIA = 15;
    final static float KOMISIASEND = 0.99f;
    final static int KOMISIASENDSUM = 5;
    final static int MAXDEPOSITMONTH = 12;


    float courseBuyUSD;
    float courseBuyEUR;
    float courseSellUSD;
    float courseSellEUR;
    float courseBuyRUR;
    float courseSellRUR;
    String addresCentralOfise;
    int yearLizance;
    int maxSumma;
    float kreditPersent;


    public Bank(String name, String addres, String addresCentralOfise, int maxSumma, float kreditPersent, int yearLizance, float courseBuyUSD, float courseBuyEUR, float courseSellUSD, float courseSellEUR, float courseBuyRUR, float courseSellRUR) {
        super(name, addres);
        this.addresCentralOfise = addresCentralOfise;
        this.courseBuyUSD = courseBuyUSD;
        this.courseBuyEUR = courseBuyEUR;
        this.courseSellUSD = courseSellUSD;
        this.courseSellEUR = courseSellEUR;
        this.courseBuyRUR = courseBuyRUR;
        this.courseSellRUR = courseSellRUR;
        this.yearLizance = yearLizance;
        this.maxSumma = maxSumma;
        this.kreditPersent = kreditPersent;


    }


    @Override
    public double send(int moneySend) {
        return moneySend * KOMISIASEND - KOMISIASENDSUM;
    }

    @Override
    public double deposit(int moneyDeposit, int moneyMonth) {
        if (moneyMonth >= MAXDEPOSITMONTH) {
            return 0;
        } else {
            return moneyDeposit;
        }
    }

    @Override
    public double kredit(int moneyKredit) {
        if (moneyKredit > maxSumma) {
            return 0;
        } else {
            return moneyKredit * kreditPersent;
        }

    }

    @Override
    public double convert(int money, String buyOrSell, String currency) {
        if (money > LIMIT) {
            return 0;
        } else if (currency.equalsIgnoreCase("EUR") && buyOrSell.equalsIgnoreCase("BUY")) {
            return money / courseBuyEUR - KOMISIA;
        } else if (currency.equalsIgnoreCase("RUB") && buyOrSell.equalsIgnoreCase("BUY")) {
            return money / courseBuyRUR - KOMISIA;
        } else if (currency.equalsIgnoreCase("USD") && buyOrSell.equalsIgnoreCase("BUY")) {
            return money / courseBuyUSD - KOMISIA;
        } else if (currency.equalsIgnoreCase("EUR") && buyOrSell.equalsIgnoreCase("SELL")) {
            return money / courseSellEUR - KOMISIA;
        } else if (currency.equalsIgnoreCase("RUB") && buyOrSell.equalsIgnoreCase("SELL")) {
            return money / courseSellRUR - KOMISIA;
        } else if (currency.equalsIgnoreCase("USD") && buyOrSell.equalsIgnoreCase("SELL")) {
            return money / courseSellRUR - KOMISIA;
        } else {
            return 0;
        }

    }

    @Override
    public void print() {
        super.print();
        System.out.println("Год лицензии: " + yearLizance + " год");
        System.out.println("Размер комисии при обмене: " + KOMISIA + " гривень");
    }
}

