package sourceit.firm;


public class Exchanger extends Organization implements sourceit.operation.Exchanger {
    float courseBuyUSD;
    float courseBuyEUR;
    float courseSellUSD;
    float courseSellEUR;

    public Exchanger(String name, String addres, float courseBuyUSD, float courseBuyEUR, float courseSellUSD, float courseSellEUR) {
        super(name, addres);
        this.courseBuyUSD = courseBuyUSD;
        this.courseBuyEUR = courseBuyEUR;
        this.courseSellUSD = courseSellUSD;
        this.courseSellEUR = courseSellEUR;
    }

    @Override
    public double convert(int money, String buyOrSell, String currency) {
        if (currency.equalsIgnoreCase("EUR") && buyOrSell.equalsIgnoreCase("BUY")) {
            return money / courseBuyEUR;
        } else if (currency.equalsIgnoreCase("USD") && buyOrSell.equalsIgnoreCase("BUY")) {
            return money / courseBuyUSD;
        } else if (currency.equalsIgnoreCase("EUR") && buyOrSell.equalsIgnoreCase("SELL")) {
            return money / courseSellEUR;
        } else if (currency.equalsIgnoreCase("USD") && buyOrSell.equalsIgnoreCase("SELL")) {
            return money / courseSellUSD;
        } else {
            return 0;
        }
    }
}


