package sourceit;


import sourceit.firm.*;

import java.lang.reflect.Array;
import java.util.ArrayList;


public final class Generator {

    private Generator() {
    }
    public static ArrayList generate() {
        ArrayList organizations = new ArrayList(7);
        organizations.add(new Bank("БАНК: 'Приват-Банк'", "адресс неизвестен", "г.Днепропетровск, ул. Гоголя,2А", 200000, 1.25f, 2005, 2.5f, 90.5f, 26.5f, 26.5f, 40.5f, 26.5f));
        organizations.add(new Exchanger("ОБМЕНКА: 'Чистые деньги'", "г. Харьков, ул.Шатиловская,8", 10.5f, 90.5f, 26.5f, 26.5f));
        organizations.add(new Post("ПОЧТА: 'Happy days'", "г. Харьков, ул.Авиационная,3"));
        organizations.add(new Pif("ПИФ: 'Happy people'", "г. Харьков, ул.Нагорная,5", 2003));
        organizations.add(new Lender("КРЕДИТ КАФЕ: 'Happy man'", "г. Харьков, ул.Нагорная,6", 4000, 2f));
        organizations.add(new Lender("КРЕДИТ СОЮЗ: 'Happy woman'", "г. Харьков, ул.Нагорная,9", 100000, 1.2f));
        organizations.add(new Lender("ЛОМБАРД: 'Happy children'", "г. Харьков, ул.Нагорная,12", 50000, 1.4f));
        return organizations;
    }
}