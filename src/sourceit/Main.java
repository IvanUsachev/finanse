package sourceit;


import sourceit.firm.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {


        FinanceSystem financeSystem = new FinanceSystem(Generator.generate());
        Scanner scanner = new Scanner(System.in);
        System.out.println("Hello ;)");
        System.out.println("Выберите нужную операцию: покупка(Buy), продажа (Sell)");
        String buyOrSell = scanner.nextLine();
        System.out.println("С какой валютой будем работать: доллары(USD), евро(EUR), рубли (RUB)");
        String currency = scanner.nextLine();
        System.out.println("Введите емеющеюся сумму денег");
        int money = Integer.parseInt(scanner.nextLine());
        financeSystem.printBestExchanger(currency, buyOrSell, money);
        System.out.println("Введите емеющеюся сумму денег для пересылки");
        int moneySend = Integer.parseInt(scanner.nextLine());
        financeSystem.printBestSender(moneySend);
        System.out.println("Введите емеющеюся сумму денег для вложения:");
        int moneyDeposit = Integer.parseInt(scanner.nextLine());
        System.out.println("Введите количетсво месяцев вложения:");
        int moneyMonth = Integer.parseInt(scanner.nextLine());
        financeSystem.printBestDeposit(moneyDeposit, moneyMonth);
        System.out.println("Введите необходимую сумму которую хотите получить в кредит:");
        int moneyKredit = Integer.parseInt(scanner.nextLine());
        financeSystem.printBestKredit(moneyKredit);

    }
}

